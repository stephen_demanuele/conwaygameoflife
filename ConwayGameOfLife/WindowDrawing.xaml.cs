﻿using ConwayGameOfLife.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConwayGameOfLife
{

    public partial class WindowDrawing : Window
    {
        ushort xRows, yRows;
        readonly byte cellDimension = 3;
        readonly byte verticalOffset = 10;
        Process p;

        public WindowDrawing(Process p)
        {
            InitializeComponent();
            this.xRows = p.cells.Max(x => x.XCoord);
            this.yRows = p.cells.Max(y => y.YCoord);
            Draw(p.cells);
            this.p = p;
        }

        void Draw(List<Cell> cells)
        {
            cells.ForEach(c => DrawEllipse(c));
            gridOwner.Width = (xRows * cellDimension) + verticalOffset;
            gridOwner.Height = (yRows * cellDimension) + verticalOffset;
            this.Width = gridOwner.Width + 50;
            this.Height = gridOwner.Height + 50;
        }

        void DrawEllipse(Cell cell)
        {
            Ellipse l = new Ellipse();
            l.Margin = new Thickness(
                (cell.XCoord - 1) * cellDimension,
                ((cell.YCoord - 1) * cellDimension) + verticalOffset,
                0,
                0);
            l.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            l.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            l.Width = cellDimension;
            l.Height = cellDimension;
            l.MouseEnter += l_MouseEnter;
            //l.MouseDown += l_MouseDown;
            Binding b = new Binding("State");
            b.Converter = new BoolBrushConverter();
            l.SetBinding(Ellipse.FillProperty, b);
            l.DataContext = cell;
            gridOwner.Children.Add(l);
        }

        void l_MouseEnter(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) ((Cell)((Ellipse)sender).DataContext).Toggle();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            p.Reset();
        }

    }
}
