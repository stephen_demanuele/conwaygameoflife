﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwayGameOfLife.classes
{

    //public enum NeighbourNumber { Top = 0, TopRight = 1, Right = 2, DownRight = 3, Down = 4, DownLeft = 5, Left = 6, TopLeft = 7 }
    public enum NeighbourNumber { Top = 7, TopRight = 0, Right = 4, DownRight = 2, Down = 6, DownLeft = 1, Left = 5, TopLeft = 3 }

    public class Cell : INotifyPropertyChanged
    {

        public ushort XCoord { get; set; }

        public ushort YCoord { get; set; }

        bool state;
        public bool State
        {
            get
            {
                return state;
            }
            set
            {
                if (state != value)
                {
                    state = value;
                    PropertyChanged(this,new PropertyChangedEventArgs("State"));
                }
            }
        }

        public Cell[] Neighbours { get; set; }

        public Cell(ushort xcoord, ushort ycoord)
        {
            this.XCoord = xcoord;
            this.YCoord = ycoord;
            State = false;
            Neighbours = new Cell[8];
        }

        public void Toggle()
        {
            State = !State;
            Task.Factory.StartNew(() => PropagateToNeighbours(this));
        }

        private void PropagateToNeighbours(Cell starter)
        {
            Task.Factory.StartNew(() => Neighbours.AsParallel().ToList().ForEach(neighbour =>
            {
                if (neighbour.XCoord != starter.XCoord && neighbour.YCoord != starter.YCoord) neighbour.Update(excludeCell: starter);
            }));
            System.Threading.Thread.Sleep(50);
        }

        public void Update(Cell excludeCell)
        {
            byte neighboursAliveCount = (byte)Neighbours.Count(x => x.State);
            bool newStateShouldBe;
            if (neighboursAliveCount <= 1 || neighboursAliveCount >= 4)
            {
                newStateShouldBe = false;
            }
            else newStateShouldBe = true;//because neighboursAliveCount is 2 or 3
            
            //this will minimise calls to PropagateToNeighbours to only cells which changed
            if (newStateShouldBe != State)
            {
                State = newStateShouldBe;
                PropagateToNeighbours(this);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }

    public class Process
    {

        public ushort XSize { get; set; }

        public ushort YSize { get; set; }

        public List<Cell> cells { get; set; }

        public Process(ushort xSize, ushort ySize)
        {
            this.XSize = xSize;
            this.YSize = ySize;
        }

        public async void Init()
        {
            cells = new List<Cell>(XSize * YSize);
            for (ushort y = 1; y <= YSize; y++)
            {
                for (ushort x = 1; x <= XSize; x++)
                {
                    cells.Add(new Cell(x, y));
                }
            }

            await Task.Factory.StartNew(() => LinkNeighbours());

            WindowDrawing drawing = new WindowDrawing(this);
            drawing.ShowDialog();
        }

        public void Reset()
        {
            cells.ForEach(c => c.State = false);
        }

        private void LinkNeighbours()
        {
            cells.AsParallel().ForAll(LinkCell);
        }

        private void LinkCell(Cell cell)
        {
            ushort xTopLeft = cell.XCoord == 1 ? XSize : (ushort)(cell.XCoord - 1);
            ushort yTopLeft = cell.YCoord == 1 ? YSize : (ushort)(cell.YCoord - 1);
            cell.Neighbours.SetValue(cells.First(c => c.XCoord == xTopLeft && c.YCoord == yTopLeft), (byte)NeighbourNumber.TopLeft);

            ushort xTop = cell.XCoord;
            ushort yTop = cell.YCoord == 1 ? YSize : (ushort)(cell.YCoord - 1);
            cell.Neighbours.SetValue(cells.First(c => c.XCoord == xTop && c.YCoord == yTop), (byte)NeighbourNumber.Top);

            ushort xTopRight = cell.XCoord == XSize ? (ushort)1 : (ushort)(cell.XCoord + 1);
            ushort yTopRight = cell.YCoord == 1 ? YSize : (ushort)(cell.YCoord - 1);
            cell.Neighbours.SetValue(cells.First(c => c.XCoord == xTopRight && c.YCoord == yTopRight), (byte)NeighbourNumber.TopRight);

            ushort xRight = cell.XCoord == XSize ? (ushort)1 : (ushort)(cell.XCoord + 1);
            ushort yRight = cell.YCoord;
            cell.Neighbours.SetValue(cells.First(c => c.XCoord == xRight && c.YCoord == yRight), (byte)NeighbourNumber.Right);

            ushort xDownRight = cell.XCoord == XSize ? (ushort)1 : (ushort)(cell.XCoord + 1);
            ushort yDownRight = cell.YCoord == YSize ? (ushort)1 : (ushort)(cell.YCoord + 1);
            cell.Neighbours.SetValue(cells.First(c => c.XCoord == xDownRight && c.YCoord == yDownRight), (byte)NeighbourNumber.DownRight);

            ushort xDown = cell.XCoord;
            ushort yDown = cell.YCoord == YSize ? (ushort)1 : (ushort)(cell.YCoord + 1);
            cell.Neighbours.SetValue(cells.First(c => c.XCoord == xDown && c.YCoord == yDown), (byte)NeighbourNumber.Down);

            ushort xDownLeft = cell.XCoord == 1 ? XSize : (ushort)(cell.XCoord - 1);
            ushort yDownLeft = cell.YCoord == YSize ? (ushort)1 : (ushort)(cell.YCoord + 1);
            cell.Neighbours.SetValue(cells.First(c => c.XCoord == xDownLeft && c.YCoord == yDownLeft), (byte)NeighbourNumber.DownLeft);

            ushort xLeft = cell.XCoord == 1 ? XSize : (ushort)(cell.XCoord - 1);
            ushort yLeft = cell.YCoord;
            cell.Neighbours.SetValue(cells.First(c => c.XCoord == xLeft && c.YCoord == yLeft), (byte)NeighbourNumber.Left);

            //return cell;
        }

    }

}
