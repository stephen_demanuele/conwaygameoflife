﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ConwayGameOfLife.classes;

namespace ConwayGameOfLife
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Start(ushort rows, ushort columns)
        {
            Process p = new Process(rows, columns);
            p.Init();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            ushort y, x;
            if (ushort.TryParse(tbRows.Text, out y) && ushort.TryParse(tbColumns.Text, out x))
            {
                Start(x, y);
            }
            else MessageBox.Show("Check input for rows and columns");

        }
    }
}
